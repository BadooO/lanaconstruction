class CreateProjectsTables < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :intitule
      t.string :adresse
      t.integer :status
      t.date :date_debut
      t.date :date_fin
      t.timestamps
      t.references :customer, foreign_key: true
    end
  end
end
