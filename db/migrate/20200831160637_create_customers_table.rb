class CreateCustomersTable < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.column :cni, :bigint
      t.string :prenom
      t.string :nom
      t.string :telephone
      t.string :mail
      t.string :adresse
      t.timestamps
    end
  end
end
