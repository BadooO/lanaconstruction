class CreateDevisItemsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :devis_items_tables do |t|
      t.string :nom_item
      t.string :unite_item
      t.integer :qte_item
      t.integer :prix_unitaire_item
      t.references :devis, foreign_key: true
    end
  end
end
