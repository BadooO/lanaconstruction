class CreateDevisTable < ActiveRecord::Migration[5.2]
  def change
    create_table :devis do |t|
      t.timestamps
      t.integer :status
      t.references :project, foreign_key: true
    end
  end
end
