# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_31_163655) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.bigint "cni"
    t.string "prenom"
    t.string "nom"
    t.string "telephone"
    t.string "mail"
    t.string "adresse"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "devis", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.bigint "project_id"
    t.index ["project_id"], name: "index_devis_on_project_id"
  end

  create_table "devis_items_tables", force: :cascade do |t|
    t.string "nom_item"
    t.string "unite_item"
    t.integer "qte_item"
    t.integer "prix_unitaire_item"
    t.bigint "devis_id"
    t.index ["devis_id"], name: "index_devis_items_tables_on_devis_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "intitule"
    t.string "adresse"
    t.integer "status"
    t.date "date_debut"
    t.date "date_fin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "customer_id"
    t.index ["customer_id"], name: "index_projects_on_customer_id"
  end

  add_foreign_key "devis", "projects"
  add_foreign_key "devis_items_tables", "devis", column: "devis_id"
  add_foreign_key "projects", "customers"
end
